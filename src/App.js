import logo from './logo.svg';
import './App.css';
import Spares from './spares/';
import 'bootstrap/dist/css/bootstrap.css';

function App() {
  return (
    <div className="App">
      <Spares />
    </div>
  );
}

export default App;
