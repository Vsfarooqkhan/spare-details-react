import React, { Component } from 'react';
import { Formik } from 'formik';
import validator from 'validator';
import {Row, Col, Input,Label, Form, FormFeedback} from 'reactstrap';
import * as Yup from 'yup';
import axios from 'axios';
import Swal from 'sweetalert2';
import Flatpickr from "react-flatpickr";
import "flatpickr/dist/themes/material_blue.css";
import LaddaButton, {EXPAND_LEFT } from 'react-ladda';
import 'ladda/dist/ladda-themeless.min.css';
import api_url from '../constants.js'
//const FILE_SIZE = 160 * 1024;
//Formik validation 
const validate = (getValidationSchema) => {
  return (values) => {
    const validationSchema = getValidationSchema(values)
    try {
      validationSchema.validateSync(values, { abortEarly: false })
      return {}
    } catch (error) {
      return getErrorsFromValidationError(error)
    }
  }
}
// Get formik errors 
const getErrorsFromValidationError = (validationError) => {
  const FIRST_ERROR = 0
  return validationError.inner.reduce((errors, error) => {
    return {
      ...errors,
      [error.path]: error.errors[FIRST_ERROR],
    }
  }, {})
}
// Set inital values to formik
const initialValues = {
  spareName: "",
  warehouse : '1',
  accept: false
}
// Alpha numeric regex
const alphaNumericWithDashes = /^[\w\-\s]+$/i;
const alphaNumeric = /^[a-z0-9]+$/i;
const integer = /[0-9]+$/i;
const numeric = /^[0-9]*\.[0-9][0-9]$/i;
// Formik validation 
const validationSchema = (values) =>{
  return Yup.object().shape({
    spareName: Yup.string()
    .required('Spare Name is required!')
    .matches(alphaNumericWithDashes, 'Spare name must be alpha Numberic'),
    spareCode: Yup.string()
    .required('Spare Code is required!')
    .matches(alphaNumeric, 'Spare Code must be alpha Numberic'),
    spareDesc: Yup.string()
    .required('Spare Description is required!')
    .matches(alphaNumericWithDashes, 'Spare Description Should not contain any special chars'),
    spareModel: Yup.string()
    .required('Spare Model is required!')
    .matches(alphaNumeric, 'Spare name must be alpha Numberic'),
    spareImage: Yup
        .mixed()
        .required("Spare Image is required"),
    sparePrice : Yup.string()
    .required('Price is required')
    .matches(numeric, 'Value must be numeric , Format : 00.00'),
    qtPurchased : Yup.string()
    .required('Quantity is required')
    .matches(integer, 'Value must be an Integer')
  })
}
class AddNew extends Component {
    constructor(props) {
        super(props)
        this.state = {
            prodData : [],
            subProdData : [], 
            purchasedDate : new Date(),
            loading: false,
            spareModel : "",
            spareImage : "",
            spareName : "",
            editValue : false,
        }
    }
    componentDidMount() {
        axios.get(`${api_url}/get_product_details`).then(response => {
            if(response.data) {
                this.setState({prodData : response.data.response.data});
            }
        })
    }
    // componentWillReceiveProps(nextProps) {
    //     if (this.state.editValue !== nextProps.editValue) {
    //         this.setState({editable : true, editData : this.props.editData})
    //         this.setState({ 
    //             spareCode : this.props.editData.spare_code,
    //             spareImage : this.props.editData.spare_image,
    //             spareModel : this.props.editData.spare_model,
    //             spareName : this.props.editData.spare_name,
    //         })
    //     }
    // }
    // On Product change sub product
    onProdChange = (e) => {
        if(e.target.name == "productName") {
            axios.get(`${api_url}/get_subproduct_details?product_id=${e.target.value}`).then(response => {
                if(response.data.response.response_code === 500)
                {
                    
                    Swal.fire({
                        icon : 'error',
                        title : 'Sub Product Does not exist'
                    })
                    this.setState({ subProdData : []})
                } else if(response.data.response.response_code === 400) {
                    
                    Swal.fire({
                        icon : 'error',
                        title : 'Sub Product Does not exist'
                    })
                    this.setState({ subProdData : []})
                }
                else {
                    this.setState({subProdData : response.data.response.data})
                }  
            })
        }
        
        this.setState({ [e.target.name] : e.target.value})
    }
    //Ladda Button toggle
    toggleBtn = (name,values) => {
        this.setState({
            loading: !this.state.loading,
            progress: 0.5,
        });
        if(values !== null) {
            this.handleSubmit(values);
        }
    }
    //SUBMIT VALUES TO API
    handleSubmit = (values) => {
        if(!this.state.productName) {
            Swal.fire({
                icon : 'error',
                title : 'Please Select the Product Name',
            })
            this.toggleBtn('expLeft', null)
            this.setState({ loading : false})
        }
        else if(!this.state.subProdName) {
            Swal.fire({
                icon : 'error',
                title : 'Please select the Sub Product Name',
            })
            this.toggleBtn('expLeft', null)
            this.setState({ loading : false})
        }
        else {
            var data = {
                "spare_code": values.spareCode,
                "spare_name": values.spareName,
                "spare_desc": values.spareDesc,
                "spare_model": values.spareModel,
                "price": values.sparePrice,
                "warehouse_id": values.warehouse,
                "product_id":this.state.productName,
                "product_sub_id": this.state.subProdName,
                "quantity_purchased": values.qtPurchased,
                "purchased_date": this.state.purchasedDate,
                "expiry_date": this.state.expiryDate,
            }
            var imagefile = document.querySelector('#spareImage');
            data.spare_image = imagefile.files[0];
            console.log(data)
            axios.post(`${api_url}/add_spare/`,data, {}).then(response => {
                if(response.data.response.response_code === '200') {
                    Swal.fire({
                        icon : 'success',
                        title : response.data.response.message
                    })
                }
                this.setState({ loading : false})
                this.props.toggle()
            })
        }
    }
    render() {
        return (
            <React.Fragment>
                <Formik
                    initialValues={{    spareName: this.state.spareName, 
                                        spareModel : this.state.spareModel,
                                        warehouse : '1'}}
                    enableReinitialize
                    validate={validate(validationSchema)}
                    onSubmit={values => {
                        this.handleSubmit(values)
                    }}
                    >
                    {({
                        values,
                        errors,
                        touched,
                        handleChange,
                        handleBlur,
                        isValid,
                        handleSubmit,
                        isSubmitting,
                        status,
                        dirty,
                        handleReset,
                        setTouched
                        /* and other goodies */
                    }) => (
                            <div className = "container">
                                <Form onSubmit={handleSubmit} className = "form-group row">
                                    <Col md="6">
                                        <Label htmlFor = "spareName" className = "control-label col-md-6">Spare Name :  </Label>
                                        <Input
                                            className = "form-control col-md-6"
                                            type="text"
                                            name="spareName"
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            value={values.spareName}
                                        />
                                        <br/>
                                        <div class="text-danger">
                                            {errors.spareName && touched.spareName && errors.spareName}
                                        </div>
                                    </Col>
                                    <Col md="6">
                                        <Label htmlFor = "spareCode" className = "control-label col-md-6">Spare Code:  </Label>
                                        <Input
                                            className = "form-control col-md-6"
                                            type="text"
                                            name="spareCode"
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            value={values.spareCode}
                                        />
                                        <br/>
                                        <div class="text-danger">{errors.spareCode && touched.spareCode && errors.spareCode}</div>
                                    </Col>
                                    <Col md = "6">
                                        <Label htmlFor = "spareDesc" className = "control-label col-md-6">Spare Description:  </Label>
                                        <Input
                                            className = "form-control col-md-6"
                                            type="text"
                                            name="spareDesc"
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            value={values.spareDesc}
                                        />
                                        <br/>
                                        <div class="text-danger">{errors.spareDesc && touched.spareDesc && errors.spareDesc}</div>
                                    </Col>
                                    <Col md="6">
                                        <Label htmlFor = "spareModel" className = "control-label col-md-6">Spare Model:  </Label>
                                        <Input
                                            className = "form-control col-md-6"
                                            type="text"
                                            name="spareModel"
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            value={values.spareModel}
                                        />
                                        <br/>
                                        <div class="text-danger">{errors.spareModel && touched.spareModel && errors.spareModel}</div>
                                    </Col>
                                    <Col md="6">
                                        <Label htmlFor = "spareImage" className = "control-label col-md-6">Spare Image </Label>
                                        <input type = "file" id = "spareImage" className = "form-control" name = "spareImage" onChange={handleChange}
                                        onBlur={handleBlur}
                                        value={values.spareImage}/>
                                        <div class="text-danger">{errors.spareImage && touched.spareImage && errors.spareImage}</div>
                                    </Col>
                                    <Col md = "6">
                                        <Label htmlFor = "sparePrice" className = "control-label col-md-6">Spare Price :  </Label>
                                        <Input
                                            className = "form-control col-md-6"
                                            type="text"
                                            name="sparePrice"
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            value={values.sparePrice}
                                        />
                                        <br/>
                                        <div class="text-danger">{errors.sparePrice && touched.sparePrice && errors.sparePrice}</div>
                                    </Col>
                                    <Col md = "6">
                                        <Label htmlFor = "warehouse" className = "control-label col-md-6"> Warehouse :  </Label>
                                        <select className = "form-control"
                                        name = "warehouse"
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        value={values.sparePrice}
                                        defaultSelected = "1"
                                        >
                                            <option value = "1" defaultSelected> Local </option>
                                            <option value = "2"> Central</option>
                                            <option value = "3"> Imprest </option>
                                        </select>
                                    </Col>
                                    <Col md = "6">
                                        <Label htmlFor = "produName" className = "control-label col-md-6"> Product Name :  </Label>
                                        <select className = "form-control" onChange = { (e) => {this.onProdChange(e)}} name = "productName"
                                        >
                                            <option defaultSelected> -- Select Product -- </option> 
                                            {this.state.prodData.map(i => {
                                                return(
                                                    <option value = {i.product_id}> {i.product_name}</option>
                                                )
                                            })}

                                        </select>
                                    </Col>
                                    <Col md = "6">
                                        <Label htmlFor = "subProdName" className = "control-label col-md-6"> Sub Product Name :  </Label>
                                        <select className = "form-control" onChange = {this.onProdChange} name = "subProdName">
                                            <option defaultSelected> -- Select Sub Product -- </option> 
                                            {this.state.subProdData.map(i => {
                                                return(
                                                    <option value = {i.product_sub_id}> {i.product_sub_name}</option>
                                                )
                                            })}

                                        </select>
                                    </Col>
                                    <Col md = "6">
                                        <Label htmlFor = "qtPurchased" className = "control-label col-md-6">Quantity Purchased :  </Label>
                                        <Input
                                            className = "form-control col-md-6"
                                            type="text"
                                            name="qtPurchased"
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            value={values.qtPurchased}
                                        />
                                        <br/>
                                        <div class="text-danger">{errors.qtPurchased && touched.qtPurchased && errors.qtPurchased}</div>
                                    </Col>
                                    <Col md = "6">
                                        <Label htmlFor = "purchasedDate" className = "control-label col-md-6"> Purchased Date :  </Label>
                                        <Flatpickr
                                            className="form-control"
                                            onChange={(_, datestr) => {
                                                this.setState({ purchasedDate : datestr });
                                                }}
                                            options={{
                                                allowInput: true,
                                                dateFormat: "Y-m-d",
                                                
                                            }}
                                        />
                                        <div class="text-danger"></div>
                                    </Col>
                                    <Col md = "6">
                                        <Label htmlFor = "expiryDate" className = "control-label col-md-6"> Expiry Date:  </Label>
                                        <Flatpickr
                                            className="form-control"
                                            onChange={(_, datestr) => {
                                                this.setState({ expiryDate : datestr });
                                                }}
                                            options={{
                                                allowInput: true,
                                                dateFormat: "Y-m-d",
                                                minDate : this.state.purchasedDate,      
                                            }}
                                        />
                                        <div class="text-danger"></div>
                                    </Col>
                                    <Col md="5"/>
                                    <Col md="7">
                                        <LaddaButton
                                            type="submit"
                                            loading={this.state.loading}
                                            onClick={() => this.toggleBtn('expLeft',values )}
                                            data-color="green"
                                            id = "submit"
                                            className = "btn btn-primary"
                                            data-style={EXPAND_LEFT}
                                            >{this.state.loading ? 'Wait...' : 'Submit'}
                                        </LaddaButton>
                                    </Col>
                                </Form>
                            </div>
                    )}
                </Formik>
            </React.Fragment>
        );
    }
}

export default AddNew;