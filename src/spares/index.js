import React, { Component } from 'react';
import AddNew from './addNew/';
import SpareDetails from './spareDetails/';
import EditSpare from './editSpare/';
import {Modal, ModalBody, ModalFooter,Row, ModalHeader} from 'reactstrap';
class index extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isModalOpen : false,
            editValue : false,
        }
    }
    toggle = () => {
        this.setState({
        isModalOpen: !this.state.isModalOpen
        });
    }

    showModal = () => {
        this.setState({
        isModalOpen: true
        });
    }
    //Edit Modal 
    toggleSecondModal = () => {
        this.setState({
        isEditModalOpen: !this.state.isEditModalOpen
        });
    }

    showEditModal = () => {
        this.setState({
        isEditModalOpen: true
        });
    }
    editData = (id, data) => {
        this.setState({editValue : true, 
                        editId : id, 
                        editData : data
                    })
        this.toggleSecondModal();
        this.forceUpdate();
    }
    render() {
        return(
            <React.Fragment>
                <div className = "container">
                    <h1> Spare Details </h1>
                    <Row>
                    <button type = "button" className = "btn btn-primary" onClick = {this.toggle}> Add Spare </button>
                    </Row>
                    <Modal
                        size="lg"
                        isOpen={this.state.isModalOpen}
                        toggle={this.showModal}
                    >
                        <ModalHeader>
                            {this.state.editValue ? 'Edit Spare' : 'Add Spare'}
                        </ModalHeader>
                        <ModalBody>
                            <AddNew toggle = {this.toggle} editValue = {this.state.editValue} editData = {this.state.editData}/>
                        </ModalBody>
                        <ModalFooter>
                            <button className = "btn btn-warning" onClick = {this.toggle}> Close </button>
                        </ModalFooter>
                    </Modal>
                    {/* Edit Modal */}
                    <Modal
                        size="lg"
                        isOpen={this.state.isEditModalOpen}
                        toggle={this.showEditModal}
                    >
                        <ModalHeader>
                            {this.state.editValue ? 'Edit Spare' : 'Add Spare'}
                        </ModalHeader>
                        <ModalBody>
                            <EditSpare toggle = {this.toggleSecondModal} editValue = {this.state.editValue} editData = {this.state.editData}/>
                        </ModalBody>
                        <ModalFooter>
                            <button className = "btn btn-warning" onClick = {this.toggleSecondModal}> Close </button>
                        </ModalFooter>
                    </Modal>
                    <SpareDetails editData = {this.editData}/>
                </div>
            </React.Fragment>
        )
        
    }
}

export default index;