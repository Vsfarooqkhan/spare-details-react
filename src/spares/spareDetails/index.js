import React, { Component } from 'react';
import axios from 'axios';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import 'react-bootstrap-table/dist//react-bootstrap-table-all.min.css';
import api_url from '../constants.js';
class SpareDetails extends Component {
    constructor(props) {
        super(props)
        this.state = {
            prodData : [],
        }
    }
    componentDidMount() {
        axios.get(`${api_url}/get_spare_details`).then(response => {
            if(response.data) {
                this.setState({prodData : response.data.response.data});
            }
        })
    }
    imageFormatter = (cell, row) => {
        return (<img src ={`http://${cell}`} alt = "spare_image"/>);
    }
    showproductName = (name) => {
        return (name.product_name)
    }
    showSubProdName = (subProd) => {
        return(subProd.product_sub_name);
    }
    editData = (id) => {
        var editColumn = [...this.state.prodData].find(driver => driver.spare_id === id);
        return(
            <button value = {id} onClick = {(id) => {this.editSpareData(id.target.value, editColumn)}} className = "btn btn-warning"> Edit</button>
        )
    }
    editSpareData =(id, data) => {
        this.props.editData(id, data);
    }
    render() {
        const options = {
            headerAlign: 'center',
            noDataText: 'No data found',
            sortIndicator: true,
            hideSizePerPage: true,
            hidePageListOnlyOnePage: true,
            clearSearch: false,
            alwaysShowAllBtns: true,
            withFirstAndLast: true,
            prePage: 'Prev', // Previous page button text
            nextPage: 'Next', // Next page button text
            firstPage: 'First', // First page button text
            lastPage: 'Last', // Last page button text
            paginationShowsTotal: this.renderShowsTotal,  // Accept bool or function
            paginationPosition: 'bottom'  // default is bottom, top and both is all available
		}
	
        return (
            <React.Fragment>
                <BootstrapTable exportCSV data={this.state.prodData} version="4" striped hover pagination search options={options}>
                    <TableHeaderColumn isKey = {true} dataField="spare_code" dataSort>Spare Code</TableHeaderColumn>
                    <TableHeaderColumn dataField="spare_name" dataSort>Spare Name</TableHeaderColumn>
                    <TableHeaderColumn dataField="spare_image" dataSort dataFormat = {this.imageFormatter}>Spare Image</TableHeaderColumn>
                    <TableHeaderColumn dataField="price" dataSort>Price</TableHeaderColumn>
                    <TableHeaderColumn dataField="product" dataSort dataFormat = {this.showproductName}>Product Name</TableHeaderColumn>
                    <TableHeaderColumn dataField="sub_product" dataSort dataFormat = {this.showSubProdName}>Sub Product</TableHeaderColumn>
                    <TableHeaderColumn dataField ="spare_id" dataSort dataFormat = {this.editData}> Action </TableHeaderColumn>
                </BootstrapTable>
            </React.Fragment>
        );
    }
}

export default SpareDetails;